using Gee;

class TreeHelper{
	public unowned Node[] nodes;

	//TODO function for checking max width or height in nodes array;

	public int max_key(Set<int> keys) {
		int ret = 0;
		foreach(var vl in keys) {
			if (ret < vl)
				ret = vl;
		}
		return ret;
	}
	
	public Gee.ArrayList<unowned Node> nodes_between(Node first, Node second){
		ArrayList<unowned Node> ret = new ArrayList<unowned Node>();
		var parent_itt = this.get_parent(first).potomki.iterator();

		//find 'first' node
		while(parent_itt.next()){
			if(first.pos == parent_itt.get().index)
				break;
		}
		//check for second node and catch all nodes netween
		while(parent_itt.next()){
			if(second.pos == parent_itt.get().index)
				break;

			ret.add(nodes[parent_itt.get().index]);
		}

		return ret;
	}

	public unowned Node? get_parent(Node node){
		if(!node.predki.is_empty)
			return nodes[node.predki.to_array()[0].index];

		return null;
	}

	public TreeHelper(Node[] node) {
		nodes = node;
	}

	public bool is_leaf(Node node) {
		return node.potomki.size == 0;
	}

	public bool is_left_most(Node node) {
		if(node.predki.is_empty)
			return true;
		
		var p_arr = node.predki.to_array();
		unowned Node node_parent = nodes[p_arr[0].index];

		var p_c_arr = node_parent.potomki.to_array();
		return node == nodes[p_c_arr[0].index];
	}

	public bool is_right_most(Node node) {
		if (node.predki.is_empty)
			return true;

		var p_arr = node.predki.to_array();
		unowned Node node_parent = nodes[p_arr[0].index];

		var p_c_arr = node_parent.potomki.to_array();
		return node == nodes[p_c_arr[node_parent.potomki.size-1].index];
	}
		
	public unowned Node? previous_sibling(Node node) {
		if (node.predki.is_empty || is_left_most(node)) {
			critical(Log.METHOD.to_string() + " - return null");
			return null;
		}
		unowned Node node_parent = nodes[node.predki.to_array()[0].index];

		var p_c_arr = node_parent.potomki.to_array();
		// add chech for previous position
		for(int i = 0; i < p_c_arr.length; ++i) {
			if (p_c_arr[i].index == node.pos)
				return nodes[p_c_arr[i - 1].index];
		}
		critical(Log.METHOD.to_string() + " - return null");
		return null;
	}

	public unowned Node? next_sibling(Node node){
		if(node.predki.is_empty || is_right_most(node))
			return null;

		unowned Node node_parent = nodes[node.predki.to_array()[0].index];

		var p_c_arr = node_parent.potomki.to_array();
		// add check for next position
		for(int i = 0; i < p_c_arr.length; ++i) {
			if (p_c_arr[i].index == node.pos)
				return nodes[p_c_arr[i + 1].index];

		}
		critical(Log.METHOD.to_string() + " - return null");
		return null; 
	}
   
	public unowned Node? left_most_sibling(Node node){
		if (node.predki.is_empty) {
			critical(Log.METHOD.to_string() + " - return null");
			return null;
		}

		if (is_left_most(node))
			return node;

		unowned Node parent_node = nodes[node.predki.to_array()[0].index];

		return nodes[parent_node.potomki.to_array()[0].index];
	}

	public unowned Node? left_most_child(Node node) {
		if (node.potomki.is_empty) {
			critical(Log.METHOD.to_string() + " - return null");
			return null;
		}
		var child_itt = node.potomki.iterator();
		child_itt.next();
		return nodes[child_itt.get().index];
	}
		

	public unowned Node? right_most_child(Node node) {
		if (node.potomki.is_empty) {
			critical(Log.METHOD.to_string() + " - return null");
			return null;
		}
		return nodes[node.potomki.to_array()[node.potomki.size -1].index];
	}
}
