using Gtk;
using Gee;

//TODO: Change to bits
public enum TreeType { 
	DEFAULT_ROOT_IN_TOP = 0, // DefaultTree.vala
	DEFAULT_ROOT_IN_BOTTOM,
	DEFAULT_ROOT_IN_LEFT,
	DEFAULT_ROOT_IN_RIGHT,
	RADIAL,			// RingTree.vala
}

public enum TreeMode {
	DEFAULT = 0,
	HORIZONTAL,
	VERTICAL,
}

abstract class TreeDrawer : Object {

	public virtual TreeMode mode {get;set; default = TreeMode.DEFAULT;}
	public weak Node[]? nodes; // TODO: maybe move nodes to TreeHelper?
	public int? tree_distance;
	public int? sibling_distance;
	public int? node_size;

	public void initialization(Node node, ...) {
		var list = va_list();
		init(node, list);
	}

	public void calculate_start_position(Node node, ...) {
		var list = va_list();
		start_position(node, list);
	}

	public void calculate_final_position(Node node, ...) {
		var list = va_list();
		final_position(node, list);
	}

	protected abstract void init(Node node, va_list? args = null);
	protected abstract void start_position(Node root, va_list? args = null);
	protected abstract void final_position(Node root, va_list? args = null);
	//protected abstract void push_tree(side, degrees)
	protected weak TreeHelper helper;
}

class TreeCompositor {

	public TreeType tree_type{get;set; default = TreeType.DEFAULT_ROOT_IN_TOP;}
	public int level_distance = 100; // 50 (300)
	
	public int? sibling_distance {
		get{ return drawer.sibling_distance; }
		set{ drawer.sibling_distance = value;}
	} //25

	//change later;
	public int? maximum_node_size {
		get{ return drawer.node_size;}
		set{ drawer.node_size = value;}
	}// 152 top, 298 left

	public int? tree_distance {
		get{ return drawer.tree_distance; }
		set{ drawer.tree_distance = value;}
	} // 0

	private TreeDrawer? drawer = null;
	private TreeHelper helper;
    private weak NodeViewer node_view;
	private weak Node[] nodes;
	
    public TreeCompositor(ref NodeViewer view, ref Node[] ref_nodes) {
		node_view = view;
		nodes = ref_nodes;
		helper = new TreeHelper(ref_nodes);
		drawer = new DefaultTree(helper);
	}
	
    public TreeCompositor start(TreeType? type = null) {
		if(type != null)
			tree_type = type;
		
		if(drawer == null)
			drawer = new DefaultTree(helper);

		switch(tree_type){
			case DEFAULT_ROOT_IN_TOP:
			drawer.mode = TreeMode.VERTICAL;
			break;
				
			case DEFAULT_ROOT_IN_BOTTOM:
			drawer.mode = TreeMode.VERTICAL;
			break;

			case DEFAULT_ROOT_IN_LEFT:
			drawer.mode = TreeMode.HORIZONTAL;
			break;
				
			case DEFAULT_ROOT_IN_RIGHT:

			drawer.mode = TreeMode.HORIZONTAL;
			break;

			case RADIAL: 
			break;

			default:
			break;
		}

		drawer.nodes = this.nodes;
		drawer.initialization(nodes[0], 0);
		drawer.calculate_start_position(nodes[0]);
		drawer.calculate_final_position(nodes[0], 0);
		return this;
    }

	public void draw(){
		int node_size = 0;
		foreach(unowned Node node in nodes) {
			unowned Node? parent = helper.get_parent(node);

			//Default mode is LEFT. So for Radial and TOP node better to use 'else' statement;
			if(drawer.mode == TreeMode.HORIZONTAL){
				node_size = node.width;
			} else{ node_size = node.height;}
			
			if(parent != null){
				var parent_rec = parent.rectangle;
				node.y = parent_rec.width + parent_rec.x + level_distance;
			}
			else {
				node.y += level_distance / 2;
			}
			
			if (tree_type == TreeType.DEFAULT_ROOT_IN_LEFT)
				revert_x_to_y(node);
			
			node_view.set_node_position(node.visual_node.gtk_node, node.x, node.y);
		}
	}

	public TreeCompositor rebuild (TreeType type) {
		if (type != this.tree_type || drawer == null) {
			start(type);
		}
		else { // bullshit.
			drawer.initialization(nodes[0], 0);
			drawer.calculate_start_position(nodes[0]);
			drawer.calculate_final_position(nodes[0], 0);
		}
		return this;
	}

	private inline void revert_x_to_y(Node node) {
		int temp = node.x;
		node.x = node.y;
		node.y = temp;
	}
}
