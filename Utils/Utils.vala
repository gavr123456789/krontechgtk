using Gee;
[Print]
inline void prin(string str){
    if (str == null) debug("str == null!");
    stdout.printf (str + "\n");
}

class TimeBench : Object {
    Timer t;
    string fname;
    public TimeBench(string func_name = "noname"){
        t = new Timer();
        fname = func_name;
    }
    ~TimeBench() {
        stdout.printf(@"Time of $fname is $(t.elapsed())\n");
    }
    public void nothing(){} // STOP UNUSED WARNINGS
}

namespace Xml{
    GXml.Document temp_document_open(string filename = "load.xml") {
        var t = new TimeBench(Log.METHOD); t.nothing();
        string test_filepath = Environment.get_user_special_dir(UserDirectory.DOCUMENTS) 
            + Path.DIR_SEPARATOR_S 
            + filename;

        prin(test_filepath);
        try {
            File f = File.new_for_path (test_filepath);
            return new GXml.Document.from_file (f);
        } catch (GLib.Error e) { 
            error (e.message); 
        }
    }

    inline GXml.Document from_file_to_xmldoc(File? file) {
        if (file == null) error("no file here");
        var t = new TimeBench(Log.METHOD); t.nothing();
        return new GXml.Document.from_file (file);
    }
}

void types_from_xml_list(HashMap<string,ArrayList<string>> types_list){
    foreach(var entry in types_list.entries){
        prin("type:",entry.key);//key - тип типов
        foreach (var b in entry.value){//value - arraylist
            prin("\t",b);
        }
    }
}

