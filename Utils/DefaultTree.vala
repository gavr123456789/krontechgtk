using Gee;

class DefaultTree : TreeDrawer {

	private enum Contour {
		LEFT = 0,
		RIGHT
	}

	private TreeMode hidden_mode;

	public DefaultTree(TreeHelper helper) {
		this.helper = helper;
	}

	public override TreeMode mode {
		get{ return hidden_mode; }
		set{ hidden_mode = value;
			switch(value){
			case TreeMode.HORIZONTAL:
				if (sibling_distance == null) sibling_distance = 50;
				if (node_size == null) node_size = 298;
				if (tree_distance == null) tree_distance = 50;
				break;
			case TreeMode.VERTICAL:
				if (sibling_distance == null) sibling_distance = 25;
				if (node_size == null) node_size = 157;
				if (tree_distance == null) tree_distance = 0;
				break;
			}
		}
	}

	public override void init(Node node, va_list? args = null) {
		tree_zero_position(node, args.arg<int>());
	}

	public override void start_position(Node node, va_list? args = null) {
		var c_itt = node.potomki.iterator();
		while(c_itt.next()) {
			start_position(nodes[c_itt.get().index]);
		}

		if (helper.is_leaf(node)) {
			if (!helper.is_left_most(node)) {
				node.x = helper.previous_sibling(node).x +
				top_or_left(helper.previous_sibling(node)) + sibling_distance;
			}
			else {
				node.x = 0;
			}
		}
		else if (node.potomki.size == 1) {
			if (helper.is_left_most(node)){
				var c_f_itt = node.potomki.iterator();
				c_f_itt.next();
				node.x = nodes[c_f_itt.get().index].x;
			}
			else {
				node.x = helper.previous_sibling(node).x +
				top_or_left(helper.previous_sibling(node)) + sibling_distance;
				
				unowned Node child = nodes[node.potomki.to_array()[0].index];
				node.mod = node.x - child.x;
			}
		}
		else {
			var left_child = helper.left_most_child(node);
			var right_child = helper.right_most_child(node);
			var mid = ((left_child.x + right_child.x + top_or_left(right_child)) / 2) - (top_or_left(node) / 2);

			if (helper.is_left_most(node)){
				node.x = mid;
			}
			else { 
				node.x = helper.previous_sibling(node).x +
				top_or_left(node) + sibling_distance;
				node.mod = node.x - mid;
			}
		}
		if (node.potomki.size > 0 && !helper.is_left_most(node))
			check_collisions(node);
	}

	public override void final_position(Node node, va_list? args = null) {
		tree_final_position(node, args.arg<int>());
	}

	private void tree_zero_position(Node node, int depth){
		node.x = -2;
		node.y = depth;
		node.mod = 0;

		var c_arr = node.potomki.to_array();

		for(int i = 0; i < node.potomki.size; ++i) {
			tree_zero_position(nodes[c_arr[i].index], depth + 1);
		}
	}

	private void tree_final_position(Node node, int mod) {
		node.x += mod;
		mod += node.mod;

		var child_arr = node.potomki.to_array();
		
		foreach(var i in child_arr) {
			unowned Node child = nodes[i.index];
			tree_final_position(child, mod);
		}
	}

	private void center_node_between(Node node, Node sibling) {
		var nodes_btw = helper.nodes_between(sibling, node);
		int count = 1;
		
		if (!nodes_btw.is_empty) {
			int distance = (node.x - sibling.x) / (nodes_btw.size +1); // +1(new) // check that later;
			foreach(var midle_node in nodes_btw){
				if(!helper.is_leaf(midle_node)) continue;

				int desiredX = sibling.x + (distance * count);
				int offset = desiredX - midle_node.x;
				midle_node.x += offset + (top_or_left(midle_node) / 2);
				midle_node.mod += offset + (top_or_left(midle_node) / 2);

				count++;
			}
			check_collisions(node);
		}
	}

	private void check_collisions(Node node) {
		var min_distance = tree_distance + node_size;
		int shift_value = 0;

		var sibling = helper.left_most_sibling(node);
		
		while(sibling != null && sibling.pos != node.pos) {
			var node_left_contour = new HashMap<int, int>();
			var sibling_right_contour = new HashMap<int, int>();
			get_contour(node, 0, ref node_left_contour, Contour.LEFT);
			get_contour(sibling, 0, ref sibling_right_contour, Contour.RIGHT);

			// create container with max_key value, because we call .keys so much times
			for(int level = node.y + 1;
				 level <= int.min(helper.max_key(sibling_right_contour.keys), helper.max_key(node_left_contour.keys));
				++level) {
				var distance = node_left_contour[level] - sibling_right_contour[level];

				if(distance + shift_value < min_distance)
					shift_value = min_distance - distance;
			}

			if (shift_value > 0) {
				node.x += shift_value;
				node.mod += shift_value;
				// funtion for centering
				//if(helper.previous_sibling(node).potomki.size <= 0)
				center_node_between(node, sibling);

				shift_value = 0;
			}
			sibling = helper.next_sibling(sibling);
		}
	}

	private void get_contour(Node node, int mod, ref HashMap<int, int> values, Contour contour) {
		if (!values.has_key(node.y))
			values.set(node.y, node.x + mod);
		else {
			if (contour == Contour.LEFT)
				values[node.y] = int.min(values[node.y], node.x + mod);
			else
				values[node.y] = int.max(values[node.y], node.x + mod);
		}
		mod += node.mod;
		var child_itt = node.potomki.iterator();
		while(child_itt.next()) {
			var child_node = nodes[child_itt.get().index];
			get_contour(child_node, mod, ref values, contour);
		}
	}

	//change TreeDrawer, when RadialTree has be added.
	private inline int top_or_left(Node node) {
		if (hidden_mode == TreeMode.VERTICAL)
			return node.width;
		else
			return node.height;
	}
} 
