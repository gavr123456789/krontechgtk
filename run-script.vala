#!/usr/bin/env -S vala --pkg json-glib-1.0
const string ENVSET[] = {"GSETTINGS_SCHEMA_DIR=build/data/", "GTK_DEBUG=interactive"};
const string MESONOUT = "build/meson-out";
void main(){
    set_env();
    //if yes then only ninja -C build
    //if not then meson build && ninja -C build
    meson_ninja(check_if_file_exist());
    //find exec file from json (build/meson-info/intro-targets.json)
    var execpath = find_exec_from_json();
    //check if meson-out folder exist
    var meson_out_exist = check_if_file_exist(MESONOUT);//TODO not sure about windows here
    //if exist than get exec name frompath and exec it from build/meson-out
    if (meson_out_exist) execpath = MESONOUT + get_exec_name_from_path(execpath);
    message(@"tying to exec $execpath");
    assert(check_if_file_exist(execpath));
    try {Process.spawn_command_line_sync(execpath);} catch (SpawnError e) {error(e.message);}

}

inline void set_env(){
    foreach (var thing in ENVSET){
        var envarr = thing.split("=");
        assert (envarr.length == 2);
        Environment.set_variable(envarr[0], envarr[1], true);
    }
}

inline bool check_if_file_exist(string build_dir_name = "build"){
    return File.new_for_path(build_dir_name).query_exists();
}

inline void meson_ninja(bool build_exitst) {
    int ninja_return = 0;
    string std_error;
    string std_out;
    try {
        if (build_exitst)
            Process.spawn_command_line_sync("ninja -C build", out std_out, out std_error, out ninja_return);
        else {
            Process.spawn_command_line_sync("meson build");
            Process.spawn_command_line_sync("ninja -C build", out std_out, out std_error, out ninja_return);
        }
    } catch (SpawnError e) {error(e.message);}

    if (ninja_return != 0) error(std_out);
}

inline string find_exec_from_json(){
    const string DIRSEP = Path.DIR_SEPARATOR_S;
    string path = string.join(DIRSEP,"build","meson-info","intro-targets.json");
    if (check_if_file_exist(path)){
        var parcer = new Json.Parser();;
        Json.Node?  node;
        Json.Array  array;
        string? target_type = "";
        string? filename = "";

        parcer.load_from_file(path);
        node = parcer.get_root();
        array = node.get_array();

        array.foreach_element((arr,index,elem)=>{
            var o = elem.get_object();
            node = o.get_member("type");
            target_type = (node != null) ? node.get_string() : null;
            if (target_type == "executable") {
                node = o.get_member("filename");
                filename = (node != null) ? node.get_array().get_element(0).get_string() : null;
                return;
            }
        });

        if (filename != null && target_type == "executable"){
            prin("Found target type: ", target_type);
            prin("Found target exec file: ", filename);
            return filename;
        } else error("Exec not found");
            
    } else {
        error(@"$(string.join(DIRSEP,"build","meson-info","intro-targets.json")) does not exists");
    }
}

inline string get_exec_name_from_path(owned string path){
    int i = path.last_index_of (Path.DIR_SEPARATOR_S);
    return path[i:path.length];
}

[Print]
void prin (string s) {
    stdout.printf (s + "\n");
}
