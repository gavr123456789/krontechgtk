
# Contribution guide  

## Where to place new code
 - If this is widget with new functionality place new file in Widgets for it. If this new functionality depends on node type of something simular than add this functionality via factory pattern, with new file in Controllers created. All default funcs must be added in widget construct section.
 - If this is a thing that can help in any part of project than make it as pure func in Utils.  
 - If this is a thing that can be a lot of copies, add its model to Models.


### Recommended extensions for Visual Studio Code
- [Trailing Spaces](https://marketplace.visualstudio.com/items?itemName=shardulm94.trailing-spaces)
- [Meson](https://marketplace.visualstudio.com/items?itemName=asabil.meson)
- [Bracket Pair Colorizer 2](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2)
- [Vala Grammar](https://marketplace.visualstudio.com/items?itemName=philippejer.vala-grammar)
- [Vala Language Client](https://marketplace.visualstudio.com/items?itemName=philippejer.vala-language-client)
- [Insert Numbers](https://marketplace.visualstudio.com/items?itemName=Asuka.insertnumbers)
- [Uncrustify](https://marketplace.visualstudio.com/items?itemName=LaurentTreguier.uncrustify)
- [vscode-icons](https://marketplace.visualstudio.com/items?itemName=vscode-icons-team.vscode-icons)
- [Todo Tree](https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.todo-tree)
- [XML](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-xml)


## Coding style

### 1. Identifiers
1. All identifiers should be given a meaningful, english name
2. Class name identifiers should be written in **UpperCamelCase**
3. Class members and local variables should be written in **snake_case**

### 2. Indentation and Spaces
1. 1 tab = 4 spaces
2. Always strip trailing spaces (if you are using VSCode, [Trailing Spaces](https://marketplace.visualstudio.com/items?itemName=shardulm94.trailing-spaces) extension can do this for you)

### 3. Brackets and Operators
1. Use [K&R style](https://en.wikipedia.org/wiki/Indentation_style#K&R_style) without mandatory braces.
3. Operators should be separated with one space.
4. Method call operator **()** with nospace style, `func()`.  
5. Seq 1 tab indented.  
```vala
Seq.of_array<string>(array)
    .parallel()
    .filter(g => g.length == 3)
    .map<string>(g => g.up())
    .foreach(g => print("%s\n", g))
    .wait();
```
