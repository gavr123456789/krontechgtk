There no License until 0.0.4 release  
  
![screenshot](krontech.png)

# What is this about?
This will be a universal Visualizer and editor for XML (and possibly other formats in the future). The XML will be displayed as a graph.
# Why?
Have you never had huge confusing XML configurations that you have to parse with your eyes?
### Use cases:
 * Management .ui files of GTK/Qt/may be any UI framework
 * HTML???
 * Ur personal configurations
# General plan  
1) Create a plan
2) ~UI or smth that looks like UI
3) Editable nodes in UI
4) DnD elements for creating new nodes with default parameters
5) Save UI nodes to XML (Serialization)
6) Deserialization from XML
7) ...
8) Profit

# Dependencies
 * [GLib](https://github.com/GNOME/glib) - Vala std
 * [GTK](https://gitlab.gnome.org/GNOME/gtk) - UI
 * Gee - Collections
 * [GTKFlow](https://notabug.org/grindhold/libgtkflow) - Flow widgets
 * GFlow - For gtkflow
 * [GXml](https://github.com/GNOME/gxml) - For XML parsing and manipulating
 * [libxml](https://github.com/GNOME/libxml2) - For gxml
 * [gpseq](https://gitlab.com/kosmospredanie/gpseq) - Everything for parallel data processing