using Gtk;
using GFlow;
using Gee;

[GtkTemplate (ui = "/com/gitlab/gavr123456789/krontechgtk/calc.ui")]
public class MyWidget : ApplicationWindow {
	[GtkChild] unowned Box main_box;
	[GtkChild] unowned Box sub_box;
	[GtkChild] unowned Box left_panel_box;
	[GtkChild] unowned Box right_panel_box;
	[GtkChild] unowned Box bottom_panel_box;
	[GtkChild] unowned ListBox left_list_box;
	[GtkChild] unowned FileChooserButton open_xml_file_btn;
	[GtkChild] unowned Gtk.Adjustment horizontal_distribution;
	[GtkChild] unowned Gtk.Adjustment vertical_distribution;
	[GtkChild] unowned Entry name_for_A;
	[GtkChild] unowned Entry name_for_B;
	[GtkChild] unowned Entry name_for_C;

	// GTK
	public NodeViewer nv;
	Gtk.ScrolledWindow sw = new ScrolledWindow(null,null);
	Revealer rev_left;
	Revealer rev_right;
	Revealer rev_bot;
	// Controllers
	NodeController nc;
	TreeCompositor tc;

	GLib.Settings settings = new GLib.Settings("com.gitlab.gavr123456789.krontechgtk"); 

	public MyWidget () {
		nv = new NodeViewer();
		nc = new NodeController(ref nv);
		
		edge_setup();
		build_ui();
		build_xml();
		fill_widgets();
		show_all();
		setup_settings();
		tests();
		
		destroy.connect (Gtk.main_quit);
	}

	void setup_settings(){
		move (settings.get_int("pos-x"), settings.get_int("pos-y"));
	}

	void setup_tree() {
		tc = new TreeCompositor(ref nv, ref nc.nodes); 
		tc.start(TreeType.DEFAULT_ROOT_IN_LEFT).draw();
	}

	void tests(){
		//  var new_node1 = nc.new_node(NodeType.B);
		//  nv.add_with_child( new_node1.visual_node.gtk_node,
		//  				   new_node1.visual_node.content);

		//  var new_node2 = nc.new_node(NodeType.B);
		//  nv.add_with_child( new_node2.visual_node.gtk_node,
		//  					new_node2.visual_node.content);

		//  nc.add_node_chain(new_node1, new_node2, Z);


		//  nc.gxml_parser();
		//nc.get_size();
	}

	void edge_setup(){
		rev_left = new Revealer();
    	rev_left.set_transition_type( RevealerTransitionType.SLIDE_RIGHT);
    	rev_left.set_transition_duration( 250 );
		rev_left.child = left_panel_box;
		rev_left.reveal_child = false;

		rev_right = new Revealer();
    	rev_right.set_transition_type( RevealerTransitionType.SLIDE_LEFT);
    	rev_right.set_transition_duration( 250 );
		rev_right.child = right_panel_box;
		rev_right.reveal_child = false;

		rev_bot = new Revealer();
    	rev_bot.set_transition_type( RevealerTransitionType.SLIDE_UP);
    	rev_bot.set_transition_duration( 250 );
		rev_bot.child = bottom_panel_box;
		rev_bot.reveal_child = false;
	}

	void build_ui () {
		horizontal_distribution.step_increment = 0.1;
		vertical_distribution.step_increment = 0.1;
		prin(horizontal_distribution.step_increment);

		main_box.pack_end (rev_bot, false, false);
		sub_box.pack_start (rev_left, false, false);
		sub_box.add (sw);
		sub_box.pack_end (rev_right, false, false);
		sw.expand = true;
		sw.add (nv);
	}

	void build_xml(){
		//Xml.gxml_recursive_test();
	}
	void fill_widgets(){
		ListBoxRow[] rows = {
			new ListBoxRow(),
			new ListBoxRow(),
			new ListBoxRow()
		};

		rows[0].add(new DropNode(){dnd_node_type = NodeType.A, label = "A"});
		rows[1].add(new DropNode(){dnd_node_type = NodeType.B, label = "B"});
		rows[2].add(new DropNode(){dnd_node_type = NodeType.C, label = "C"});

		foreach(unowned ListBoxRow i in rows){
			left_list_box.add(i);
		}
	}

	[GtkCallback]
	void any_panel_visible (ToggleButton tgbtn){
		debug(@"right panel toggle.btn = $(tgbtn.active)\n");
		Image? img = (tgbtn.get_child() as Image); 
		prin(img.icon_name);
		switch (img.icon_name) {
			case "help-contents-symbolic":{rev_bot.reveal_child = !rev_bot.reveal_child; break;}
			case "system-help-symbolic":{rev_right.reveal_child = !rev_right.reveal_child; break;}
			case "emblem-documents-symbolic":{rev_left.reveal_child = !rev_left.reveal_child; break;}
			default:{error("Wrong button label, please change icon name in code here");}
		}
	}

	//  [GtkCallback]
	//  void type_x_btn_clicked (Button button) {
	//  	var empty_xml_element = new GXml.Element();
	//  	var wat = empty_xml_element as GXml.DomElement;
	//  	Node? new_node = null;
	//  	switch (button.label) {
	//  		case "A":{new_node = nc.new_node(NodeType.A, ref wat, ""); break;}
	//  		case "B":{new_node = nc.new_node(NodeType.B, ref wat, ""); break;}
	//  		case "C":{new_node = nc.new_node(NodeType.C, ref wat, ""); break;}
	//  		default:{error("Wrong button label for type");}
	//  	}
	//  	nv.add_with_child( new_node.visual_node.gtk_node,
	//  					   new_node.visual_node.content);
	//  }

	[GtkCallback]
	void new_type_a (Button button) {
		var empty_xml_element = new GXml.Element();
		var wat = empty_xml_element as GXml.DomElement;
		Node? new_node = null;
		new_node = nc.new_node(NodeType.A, ref wat, name_for_A.buffer.text);
		
		nv.add_with_child( new_node.visual_node.gtk_node,
						   new_node.visual_node.content);
	}

	[GtkCallback]
	void new_type_b (Button button) {
		var empty_xml_element = new GXml.Element();
		var wat = empty_xml_element as GXml.DomElement;
		Node? new_node = null;
		new_node = nc.new_node(NodeType.B, ref wat, name_for_B.buffer.text);
		
		nv.add_with_child( new_node.visual_node.gtk_node,
						   new_node.visual_node.content);
	}

	[GtkCallback]
	void new_type_c (Button button) {
		var empty_xml_element = new GXml.Element();
		var wat = empty_xml_element as GXml.DomElement;
		Node? new_node = null;
		new_node = nc.new_node(NodeType.C, ref wat, name_for_C.buffer.text);
		
		nv.add_with_child( new_node.visual_node.gtk_node,
						   new_node.visual_node.content);
	}


	[GtkCallback]
	void on_file_set (FileChooserButton filechooserbutton) {
		nc.file = filechooserbutton.get_file();
		var xmldoc = Xml.from_file_to_xmldoc(nc.file);
		//TODO тут пока висит окно приветствия, и вот как файл открыт, мы меняем его на vodeview
		nc.gxml_parser(xmldoc);

		setup_tree();//always after gxml_parser
	}

	[GtkCallback]
	void vertical_distribution_value_changed(Adjustment adjustment){
		prin(adjustment.value);
		settings.set_double("vertical-distribution", adjustment.value);
	}

	[GtkCallback]
	void horizontal_distribution_value_changed(Adjustment adjustment){
		prin(adjustment.value);
		settings.set_double("vertical-distribution", adjustment.value);
	}

	[GtkCallback]
	void save_to_xml (Button btn){
		nc.save_to_xml("sas.xml");
	}
}

void main(string[] args) {
	Gtk.init (ref args);
	new MyWidget ();
	Gtk.main ();
}