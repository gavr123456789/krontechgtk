using Gtk;
using Gdk;
using GtkFlow;

const int SEND_DATA_SIZE = 1;

// We should to use COPY, because that one correct variant.
// LINK, PRIVATE, ASK - that for another D&D transactions.
Gdk.DragAction dnd_action = Gdk.DragAction.COPY;

enum DnDTarget {
	INT32,
	ROOTWIN
}

const TargetEntry[] dnd_target_list = {
    { "INT32",  0,  DnDTarget.INT32 }
};

public class NodeViewer : GtkFlow.NodeView {

    // That signal emiting, when user add new node by D&D
    // Beacause we cannot call NodeController expicitly, 
    // then we'll be use signals. Why not? 
    // Double linked classes is not allowed also like C++.
    public signal void addNewEmptyNode(NodeType type, int x_pos, int y_pos);

    construct{
        Gtk.drag_dest_set (
            this,
            DestDefaults.MOTION,
            dnd_target_list,
            dnd_action
        );

        this.drag_motion.connect(() => {return false;});
        this.drag_drop.connect(on_drag_drop);
        this.drag_data_received.connect(on_drag_data_received);
    }

    private bool on_drag_drop (Widget widget, DragContext context, int x, int y, uint time) {
        bool is_valid_drop_site = true;

        if (context.list_targets() != null) {
            var target_type = (Atom) context.list_targets().nth_data (DnDTarget.INT32);

            Gtk.drag_get_data (
                widget,
                context,
                target_type,
                time
            );

        } else {
            is_valid_drop_site = false;
            print("invalid drop site in NodeViewer D&D.");
        }

        return is_valid_drop_site;
    }

    private void on_drag_data_received (Widget widget, DragContext context,
                                        int x, int y,
                                        SelectionData selection_data,
                                        uint target_type, uint time)
    {
        bool dnd_success = false;
        bool delete_selection_data = false;

        //Future
        if (context.get_suggested_action() == DragAction.MOVE) {
            delete_selection_data = true;
        }

          switch (target_type) {
              case DnDTarget.INT32:

                // Bug: we cannot get data to uchar[]
                // Otherwise we'll take crash.
                //uchar[] drop_data = selection_data.get_data();

                // Like i'm say in DropWidget, we must receive only one byte.
                // When we'll be use more than 255 values, then need too read another bytes.
                // Also we can pack 4 bytes to one integer variable.
                NodeType type = (NodeType)selection_data.get_data()[0];
                addNewEmptyNode(type, x, y);

                dnd_success = true;
                break;
              default:
                prin("Unhandled case in NodeViewer D&D.");
                assert_not_reached ();
          }

        if (!dnd_success) {
            prin("Data transfer failed in NodeViewer D&D");
        }

        Gtk.drag_finish (context, dnd_success, delete_selection_data, time);
    }
}