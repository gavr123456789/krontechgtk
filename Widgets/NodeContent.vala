using GFlow;
using Gtk;

class MyContent : Box {
    public int custom_content = 0;// some custom content for check that all will works in future

    public Table table = new Table();
    public Table attrib_table = new Table(){vscroll_policy = MINIMUM};

    construct{
        var nodes_scroll = new ScrolledWindow(null,null);
        var attrib_scroll = new ScrolledWindow(null,null);
        var box = new Box(Orientation.HORIZONTAL, 0);
        var side_revealer = new Revealer();
        var stack = new Stack(){vexpand=true,hexpand=true,
            transition_type = StackTransitionType.OVER_LEFT_RIGHT};
        

        attrib_scroll.add(attrib_table);
        attrib_table.expand = false;

        //  attrib_table.vscroll_policy = MINIMUM;
        
        side_revealer.set_transition_type(RevealerTransitionType.SLIDE_LEFT);
        side_revealer.add(attrib_scroll);
        
        nodes_scroll.add(table);

        stack.add_named(nodes_scroll, "main_table");
        stack.add_named(attrib_scroll, "attributes_table");

        box.pack_start(stack, true, true);
        
        box.pack_start(side_revealer, false, true, 5);

        add(box);
        //  add(btn_attr);

        vexpand = true;
        expand = true;
        orientation = Orientation.VERTICAL;
        //homogeneous = true;
        height_request = 200; 
    }

    inline void set_model(){
        TreeModel temp_model;
        if(table.get_selected(out temp_model)){
            attrib_table.model = temp_model;
        }
        else{
            attrib_table.clear_model();
        }
    }

    ~MyContent(){
        prin("MyContent Destructor");
    }
}

class Table : TreeView {
    Gtk.TreeStore                  tree_store = new Gtk.TreeStore (2, typeof (string), typeof (string));
    Gtk.CellRendererText[]         cells = {new CellRendererText (), new CellRendererText ()};
    Gtk.TreeIter?                  last_iter {get;set; default = null;}
    Gee.HashMap<string, TreeStore> attrib_map = new Gee.HashMap<string, TreeStore>();
    TreeIter?                      attrib_iter {get; set; default = null;}
    bool                           first_row_for_attrib {get; set; default = true;}
    int                            number_of_attrib_childs = 0;
    construct{
        var column1 = new TreeViewColumn.with_attributes ("Node name",  cells[0], "text", 0){min_width = 50, sort_column_id = 0}; 
        var column2 = new TreeViewColumn.with_attributes ("Node value", cells[1], "text", 1){min_width = 50, sort_column_id = 1}; 
        append_column(column1);
        append_column(column2);
        add_new_row(null, " ", " ");

        model = tree_store;
        activate_on_single_click = false;
        reorderable       = true;
        enable_search     = true;
        enable_tree_lines = true;
        //  this.fixed_height_mode = true; // ускоряет таблицу предполагая что у всех строк в ней одинаковая высота
        show_expanders = true;
        expand = true;
        enable_grid_lines = TreeViewGridLines.BOTH;

        button_press_event.connect(right_mouse_event);
        // set last empty for new
        tree_store.row_changed.connect((edited_path, edited_iter) => {
            edited_path.next();

            TreeIter tmp_iter;
            bool have_next = model.get_iter(out tmp_iter, edited_path);

            if(row_is_empty(edited_iter) || have_next){
                // that isn't our business, we leave
                return;
            }

            TreeIter new_row_iter;

            if (tree_store.iter_depth(edited_iter) > 0) { // this is child
                TreeIter parent_iter;   
                tree_store.iter_parent(out parent_iter, edited_iter);
                tree_store.append(out new_row_iter,  parent_iter);
                tree_store.set(new_row_iter, 0, "", 1, "");
            }
            else { //this is root
                tree_store.append(out new_row_iter,  null);
                tree_store.set(new_row_iter, 0, "", 1, ""); // TODO Если сюда добавить текст то вылет, нипанятна                
            }
        });

        set_tooltip_column(1); // TODO сделать нормально чтобы было для каждой

        foreach(unowned Gtk.TreeViewColumn column in this.get_columns()){
            column.set_resizable(true);
        }

        for (int i = 0; i < cells.length; ++i){
            cells[i].editable  = true;
            cells[i].ellipsize = Pango.EllipsizeMode.END;
            cells[i].max_width_chars = 10;
            cells[i].width_chars     = 10;
            int current_column = i;

            cells[i].edited.connect ((path, new_text) => {

                TreeIter iter_current;
                bool iter_is_valid = model.get_iter(out iter_current, new Gtk.TreePath.from_string(path));

                if (iter_is_valid && !is_first_attrib_row(iter_current)) {
                    tree_store.set(iter_current, current_column, new_text);
                }

                var next_path = new Gtk.TreePath.from_string(path);
                next_path.next();

                if(tree_store.iter_depth(iter_current) > 0){ // check depth. 0 - root; >0 - child of root

                    TreeIter bad_iter;
                    if(!model.get_iter(out bad_iter, next_path) && !row_is_empty(iter_current)){ // если нет следующей строки и текущая не пустая

                        TreeIter iter_parent;
                        TreeIter iter_after;

                        //get parent iter of selected column
                        tree_store.iter_parent(out iter_parent, iter_current);

                        //create sibling with explicit parent
                        tree_store.insert_after(out iter_after, iter_parent, iter_current);
                        tree_store.set(iter_after, 0, "", 1, "");
                    }
                }
                else{ // root

                    TreeIter bad_iter;
                    if(!model.get_iter(out bad_iter, next_path) && !row_is_empty(iter_current)){ // если нет следующей строки и текущая не пустая
                        //create new row after all;
                        tree_store.append (out iter_current, null);
                        tree_store.set    (iter_current, 0, "", 1, "");
                    }
                }
            });
        }

    }

    public TreeIter add_attrib_row(){
        TreeIter iter_attrib;

        tree_store.prepend(out iter_attrib, null);
        tree_store.set(iter_attrib, 0, "Attributes", 1, "0");

        attrib_iter = iter_attrib;
        first_row_for_attrib = true;

        return attrib_iter;
    }

    public TreeIter add_attrib_table(string name, string value){
        number_of_attrib_childs++;
        tree_store.set(attrib_iter, 1, number_of_attrib_childs.to_string());
        return add_attrib_for_child_table("Attributes", name, value);
    }

    public TreeIter add_attrib(string name, string value){
        number_of_attrib_childs++;
        tree_store.set(attrib_iter, 1, number_of_attrib_childs.to_string());
        return add_attrib_for_child(attrib_iter, name, value);
    }

    public TreeIter add_new_row(TreeIter? parent, string first, string second){
        TreeIter added_iter;

        tree_store.insert(out added_iter, parent, tree_store.iter_n_children(parent) -1);
        tree_store.set(added_iter, 0, first, 1, second);

        this.last_iter = added_iter;
        return added_iter;
    }

    public TreeIter add_attrib_for_child(TreeIter parent, string first, string second) {
        TreeIter added_iter;

        tree_store.append(out added_iter, parent);
        tree_store.set(added_iter, 0, first, 1, second);

        return added_iter;
    }

    public TreeIter add_attrib_for_child_table(string child, string key, string value){
        TreeIter added_iter;
        if(!attrib_map.has_key(child)){
            attrib_map[child] = new TreeStore(2, typeof (string), typeof (string));
        }

        attrib_map[child].append(out added_iter, null);
        attrib_map[child].set(added_iter, 0, key, 1, value);

        return added_iter;
    }

    public bool get_selected(out TreeModel? attrib_model){
        TreePath path;
        TreeIter temp_iter;
        attrib_model = null;
        get_cursor(out path, null);
        if(path == null) {return false;}
        
        model.get_iter(out temp_iter, path);

        if(tree_store.iter_is_valid(temp_iter)){
            string buf;
            tree_store.get(temp_iter,0, out buf, -1);

            if(attrib_map.has_key(buf)){
                attrib_model = attrib_map[buf];
                return true;
            }
        }
        return false;
    }

    public void clear_model(){
        this.model = new Gtk.TreeStore (2, typeof (string), typeof (string));
    }

    inline bool row_is_empty(TreeIter itt){
        string buf1;
        string buf2;

        tree_store.get(itt, 0, out buf1, -1);
        tree_store.get(itt, 1, out buf2, -1);

        return (buf1 == "" && buf2 == "");
    }

    inline bool is_first_attrib_row (TreeIter iter){
        if(first_row_for_attrib)
            return (iter == attrib_iter);
        
        return false;
    }

    private bool right_mouse_event(Gtk.Widget w, Gdk.EventButton e) {
        Gtk.TreeIter? selected_iter;
        var model_ref = this.model;

        if(e.button == 3){
            var rect = Gdk.Rectangle()  {x = (int)e.x, y = (int)e.y + 25,
                                         width = 1, height = 1 };
            var pop  = new PopoverMenu(){pointing_to = rect, relative_to = w,
                                         position = RIGHT, modal = true};
            var box  = new Gtk.Box(Orientation.VERTICAL,0);
            
            var itm_add_new    = new ModelButton(){text = "Add new"};
            var itm_add_child  = new ModelButton(){text = "Add child"};
            var itm_add_after  = new ModelButton(){text = "Insert after"};
            var itm_add_before = new ModelButton(){text = "Insert before"};
            var itm_delete     = new ModelButton(){text = "Delete"};
            
            box.add(itm_add_new   );
            box.add(itm_add_child );
            box.add(itm_add_after );
            box.add(itm_add_before);
            box.add(itm_delete    );     
            pop.add(box);
            
            var tree_selection = this.get_selection();

            if(tree_selection != null){
                var have_selected = tree_selection.get_selected(out model_ref, out selected_iter);

                if(selected_iter != null && have_selected){

                    itm_add_new.clicked.connect(() => {
                        add_new_row(null, "", "");
                    });
                    itm_add_child.clicked.connect(() => {
                        add_new_row(selected_iter, "", "");
                    });
                    itm_add_after.clicked.connect(() => {
                        tree_store.insert_after(out selected_iter, null, selected_iter);
                        tree_store.set(selected_iter, 0, "", 1, "");
                    });
                    itm_add_before.clicked.connect(() => {
                        tree_store.insert_before(out selected_iter, null, selected_iter);
                        tree_store.set(selected_iter, 0, "", 1, "");
                    });
                    itm_delete.clicked.connect(() => {
                        tree_store.remove(ref selected_iter);
                    });

                    pop.popup();
                    box.show_all();
                }
            }
        }

        return false;
    }
}
