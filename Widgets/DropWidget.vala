using Gtk;
using Gdk;
using GtkFlow;

public class DropNode : Gtk.Button{

    // Default state -1 because NodeType.A = 0.
    // -1 that is normal value for exceptions/undefined data.
    public int dnd_node_type = -1;

	construct {
		Gtk.drag_source_set (
            this,
            Gdk.ModifierType.BUTTON1_MASK,
            dnd_target_list,
            dnd_action
		);

		this.drag_data_get.connect(on_drag_data_get);
	}

    private void on_drag_data_get (Widget widget, Gdk.DragContext context,
                                   SelectionData selection_data,
                                   uint target_type, uint time)
    {
        switch (target_type) {
            case DnDTarget.INT32:

                // We can zip all integers up to 255 number;
                // After 255 data shall be overridden.
                // i.e: 255 = 0xff, 256 == 0, 257 = 0.

                // Change that code, 
                // when count of NodeTypes will be >= 254.
                uchar dnd_char = (uchar)dnd_node_type;
                uchar [] buf = {dnd_char};

                // ATTENTION: we send only one byte of memory!
                // So we MUST receive ONE BYTE! 
                // Another data - trash.

                selection_data.set (
                    selection_data.get_target(),
                    SEND_DATA_SIZE,
                    buf
                );
                break;

            default:
            	prin("Unhandled case in DropWidget D&D.");
                assert_not_reached ();
        }
    }
}
