int main(string[] args){
    Gtk.init(ref args);
    Test.init (ref args);
    Test.bug_base ("https://gitlab.com/gavr123456789/krontechgtk/-/issues/");
    Test.add_func ("/new_node", () => {
        Test.bug("32");
        var nc = new NodeController();
        var a = nc.new_node(NodeType.A, "data");
        assert (a.data == "data");
        assert (a.visual_node.gtk_node != null);
        assert (nc.nodes[0] != null);

        assert (nc.nodes[0].data == "data");
    });
    Test.add_func ("/delete_node", () => {
        var nc = new NodeController();
        var a = nc.new_node(NodeType.A);
        assert (nc.nodes[0] != null);
        nc.delete_node(a);
        assert (nc.nodes[0] == null);

    });
    return Test.run ();
    //  var nc = new NodeController();
    //  var (a,b,c) = new Node[]{nc.new_node(NodeType.A),nc.new_node(B),nc.new_node(C)};
    //  nc.add_node_chain(a,b,Z);
    //  nc.add_node_chain(b,c,Z);
    //  nc.add_node_chain(c,a,Z);
    //  prin(a);
    //  prin(b);
    //  prin(c);
    //  nc.delete_node(a);
    //  prin("after delete");
    //  prin(a);
    //  prin(b);
    //  prin(c);
}