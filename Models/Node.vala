using GXml;
using Gee;
using GFlow;

public enum ChainType {
    Z, X, Y;
}
public enum NodeType {
    A, B, C;
}

// структура представляющая собой связь между нодами
struct Chain {
    int index;
    ChainType type;
    public string to_string(){
        return @"(index = $index, type = $(type))";
    }
}

// A combination of the node widget and its content(Table)
struct VisualNode {
    SimpleNode gtk_node;
    MyContent content;
	Cairo.RectangleInt position;
}

class Node {
    public Node(VisualNode _visual_node, ref GXml.DomElement xml_element, string _node_name){
        xml_dom_elem = xml_element;  visual_node = _visual_node; node_name = _node_name;
    }
    public string node_name = "";
    public int pos; // index in NodeController
    public NodeType type;
    public GXml.DomElement xml_dom_elem {get; private set;}

    public string? name{get{ return visual_node.gtk_node.name; }}
    public Cairo.RectangleInt rectangle {get{ return visual_node.position;} set{ visual_node.position = value;}}
    
	public int x{ get{ return visual_node.position.x;} set{ visual_node.position.x = value;}}
	public int y{ get{ return visual_node.position.y;} set{ visual_node.position.y = value;}}

	public int width{ get{ return visual_node.position.width;} set{ visual_node.position.width = value;}}
	public int height{ get{ return visual_node.position.height;} set{ visual_node.position.height = value;}}

    public int mod = 0;
    

    public VisualNode visual_node;
	
    // Actually parents
    public HashSet<Chain?> predki = new HashSet<Chain?>(
                (n) => {return n.index;},
                (n1, n2) => {if (n1.index == n2.index && n1.type == n2.type) return true;
                             else return false;});
    //Actually childs
    public Gee.ArrayList<Chain?> potomki = new Gee.ArrayList<Chain?>(
                (n1, n2) => {if ( n1.index > n2.index && n1.type == n2.type) return true;
                             else return false;});

    // Contains service indexes of those who point to this node, to correctly delete links from the pointing nodes.
    public HashSet<int> chains_to_this = new HashSet<int>();
  
    public void add_new_sink(){
        message(@"type == $(this.type)");
        //Если у текущей ноды нет синков то добавить один
        
        if (this.type==A){
            message("trying to add");
            var sink = new GFlow.SimpleSink(Value(Type.INT))    {name = "Parents"};
            try {
                visual_node.gtk_node.add_sink(sink);
            } catch (GFlow.NodeError e) {error(e.message);}
        }
    }

    public string to_string() {
        var builder = new StringBuilder(@"pos = $pos\npredki [\n");
        predki.foreach ((elem) => {builder.append(@"\t$elem"); return true;});
        builder.append("]\npotomki [\n");
        potomki.foreach((elem) => {builder.append(@"\t$elem"); return true;});
        builder.append("]\nchains_to_this [\n");

        chains_to_this.foreach((elem) => {builder.append(@"\t$elem"); return true;});
        builder.append("\n]");
        builder.append("------XML------");
        try {
            builder.append(xml_dom_elem.write_string());    
        } catch (GLib.Error e) {
            error(e.message);
        }
        return builder.str;
    }
}