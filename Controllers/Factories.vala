using Gee;
using Gtk;
// its temp
namespace GS{
    static int node_num   = 1;
}

/// Create Node from visual and model representation, XML data
inline Node node_factory(NodeType node_type, ref GXml.DomElement el, string node_name){
    return new Node({gtkflow_node_factory(node_type, node_name),
                     content_factory(node_type)},
                     ref el,
                     node_name)
                    {type = node_type};
}

/// Create GFlow.SimpleNode from type
GFlow.SimpleNode gtkflow_node_factory (NodeType nodeType, string node_top_name){
    var n = new GFlow.SimpleNode () {name = node_top_name != ""? node_top_name: @"node$(GS.node_num++)"};
    
    //TODO until https://notabug.org/grindhold/libgtkflow/issues/12
    //  n.button_press_event.connect(event_that_add_doubleclick_name_change);
    try {
        switch (nodeType){
            case A:{
                var source = new GFlow.SimpleSource.with_type(Type.INT){
                    name = (node_top_name=="")?node_top_name:@"Childrens"
                    };
                n.add_source(source);
                return n;
            }
            case B:{
                var sink = new GFlow.SimpleSink.with_type(Type.INT)    {name = "Parents", max_sources = 1000};
                var source = new GFlow.SimpleSource.with_type(Type.INT){ name = "Childrens"};
                n.add_source(source);
                n.add_sink(sink);
                return n;
            }
            case C:{
                var sink = new GFlow.SimpleSink.with_type(Type.INT)    {name = "Parents", max_sources = 1000};
                n.add_sink(sink);
                return n;
            }
        }

    } catch (GFlow.NodeError e){
        error("Error: " + e.message);
    }
    assert_not_reached();
}

MyContent content_factory(NodeType nodeType, int count = 0){
    var content_grid = new MyContent();

    switch (nodeType){
        case A:{
            return content_grid;
        }
        case B:{
            return content_grid;
        }
        case C:{
            //  content_grid.add(new Label("C"));
            return content_grid;
        }
    }
    assert_not_reached();
}
