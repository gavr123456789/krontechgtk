using Gee;
using Gtk;
// для регулирования расположения новых нод
struct Point {
    int x;
    int y;
}

class NodeController {
    public Node?[]                  nodes;// this public for unit tests only
    private UnrolledLinkedList<int> holes = new UnrolledLinkedList<int>();
    private Point                   new_node_pos;
    public int                      next_index = 0;
    public File?                    file;

    private weak NodeViewer nv;

    public NodeController (ref NodeViewer _nv){
        nv = _nv;
        nv.addNewEmptyNode.connect(emptyNodeHandler);
    }

        // Add handler for signal from NoveViewer.vala, 
        // when user add empty node by D&D
        


    /*
    * Save all graph to XML
    */
    public void save_to_xml(string filename){
        string content;
        if (nodes[0] != null){
            content = nodes[0].xml_dom_elem.write_string();

            FileUtils.set_contents(filename,content);
            message(nodes[0].to_string());

        }
    }

    /*
    * Debug info about sizes
    */
    public void get_size(){
        foreach(var node in nodes)
            if (node !=  null){
                prin(node.visual_node.content.name);
                var pos = nv.get_node_position(node.visual_node.gtk_node);
                prin("Node: ", node.name ," height: ", node.height, " width: ", node.width, "x: ", pos.x, " y: ", pos.y, " mod: ", node.mod);
            }
    }

private void emptyNodeHandler(NodeType type, int x_pos, int y_pos){
    var empty_xml_element = new GXml.Element();
    var wat = empty_xml_element as GXml.DomElement;
        
    Node new_node = new_node(type, ref wat, "");

    this.nv.add_with_child( new_node.visual_node.gtk_node,
        new_node.visual_node.content);

    this.nv.set_node_position(new_node.visual_node.gtk_node, x_pos, y_pos);
}

    
    /*
    * Create new node, if there are holes then put new node in it
    *
    * Replace second parameter to nullable reference?
    * Need to change.
    */
    public Node new_node(NodeType node_type, ref GXml.DomElement el, string node_name){
        bool no_holes = holes.size == 0;
        // If there are holes in the array, then put a new node in their place
        int new_index = no_holes? next_index++: holes.poll();
        var new_node = node_factory(node_type, ref el, node_name);
        new_node.pos = new_index;

        if (no_holes) nodes += new_node;
        else if (nodes[new_index] == null) nodes[new_index] = new_node;
        else assert_not_reached();

        return new_node;
    }

    /*
    * Add chain from node1 to node2, all chains have single directions
    */
    public void add_node_chain (Node node1, Node node2, ChainType chaintype = Z){
        node1.potomki.add (Chain(){ index = node2.pos, type = chaintype});
        node2.predki.add (Chain(){ index = node1.pos, type = chaintype});
        node2.chains_to_this.add(node1.pos);//добавляем в память ноды все ноды которые односторонне на нее указывают для удаления
        //  prin("А ПРОВЕРКА ТО ПРОШЛА? ", node1.type != C && node2.type != A);
        //FIXME check if there only one sink when https://notabug.org/grindhold/libgtkflow/issues/11 will done
        /// IF NODE DONT HAVE SINK AND TYPE A AND NEED TO BE LINKED AS SECOND => ADD SINK TO IT
        if (node2.type == A){
            node2.add_new_sink();
        }
        //проверка типов, у A не может быть sinc, у C не может быть source
        if (node1.type != C){
            GFlow.Source source1 = node1.visual_node.gtk_node.get_sources().first().data;

            GFlow.Sink sink2 = node2.visual_node.gtk_node.get_sinks().first().data;
            try{ source1.link(sink2); } catch(GLib.Error e){
                error(e.message);
            }
        }
    }

    /*
    * Delete this node, and delete it from all who chained to it
    */
    public void delete_node(Node node){
        prin("\ndeleting node with pos", node.pos);
        holes.add(node.pos);
        // удаление односторонних связей к элементу всех типов
        prin("удаление односторонних связей к элементу всех типов");
        foreach (var item in node.chains_to_this)
            foreach (var chain2potomok_not_chained in nodes[item].potomki)
                if (chain2potomok_not_chained.index == node.pos){
                    nodes[item].potomki.remove(chain2potomok_not_chained);
                    nodes[item].chains_to_this.remove(node.pos);// это точно нужно??? подумай на досуге
                    // вроде да, это удаление удаляемой ноды из системного списка нод указывающих на ноду из системного списка нод удаляемой ноды
                    // если А в ответ указывала бы на С то обе ноды были бы в списках друг друга, и при удалении А ... она бы 
                    // осталась в списке С как нода указывающяя на нее, что зло

                    prin("Нода удалена среди потомков ");
                    break;
                }
            
        //удаление этой ноды среди предков ее потомков
        prin("удаление этой ноды среди предков ее потомков");
        foreach (var item in node.potomki)
            if (item.index == node.pos)
                nodes[item.index].predki.remove(item);

        nodes[node.pos] = null;// в случае ошибок эту строчку коментить первой ^^
    }

    /*
    * Go throw all XML file and create node from each of it
    */
    public void gxml_parser(GXml.Document root_node = Xml.from_file_to_xmldoc(file)){
        var t = new TimeBench(Log.METHOD); t.nothing();

        var main_array = new Gee.LinkedList<GXml.DomNode>();
        var temp_array = new Gee.ArrayList<GXml.DomNode>();
        var map = new HashMap<GXml.DomElement, Node>();//мапа уже посещенных нод по именнам, для связывания к ним потомков

        //rootnode content
        prin("root node: ", root_node.node_name, " : ",
                root_node.text_content != null? root_node.text_content: "no content, is DomElement? :",
                (root_node is GXml.DomElement),"\n");
        //добавляем первую в рут, дальше идем по остаткам её детей
        main_array.add(root_node);

        //АЛГОРИТМ, создаем хешмап уже добавленных по именам, и затем будучи в детях связываем эту ноду с нодой из хешмапы по именам
        while (!main_array.is_empty) {
            // добавить потомков текущей, удалить текущую
            while (!main_array.is_empty) {
                var temp = main_array.peek();
                ///DEBUG
                //  prin("NODE TYPE: ", temp.node_type);
                //  prin("current level length: ", main_array.size);
                //  if (temp is GXml.DomElement && temp.text_content != null && temp.parent_element != null)
                //      prin("deleted node: ", temp.node_name, " : ",
                //              temp.text_content.strip(),"\n",
                //              "Parent: ", temp.parent_element.node_name);
                        

                ///MAIN PROCESSING
                if (temp is GXml.DomElement){
                    var temp_dom = (GXml.DomElement)temp;
                    
                    // CREATES new_node depends on TYPE RECOGNITION
                    //FIXME разбить на мелкие функции
                    if (temp_dom.child_element_count != 0){
                        NodeType node_type = type_recognition(temp_dom);
                        prin("recognized type: ", node_type,"\n-----------\n");
                        Node new_node = add_to_node_view(node_type, ref temp_dom, temp_dom.node_name);
                        debug(new_node.to_string());

                        if (temp_dom.parent_element != null && map.has_key(temp_dom.parent_element)) // we actually do not need to check father node;
                            add_node_chain(map[temp_dom.parent_element], new_node);

                        map[temp_dom] = new_node;

                        var attr = temp_dom.attributes;
                        if(!attr.is_empty){
                            new_node.visual_node.content.table.add_attrib_row();
                            foreach(var itt in attr.entries)
                                new_node.visual_node.content.table.add_attrib(itt.key, itt.value.node_value);
                        }
                        //warning(attr.size.to_string());
                    } else {
                        /// THIS IS THE CONTENT OF THE NODE, IT DOES NOT NEED A VISUAL NODE
                        var attr = temp_dom.attributes;
                        TreeIter subrow_iter;
                        
                        if (temp_dom.text_content != null)
                            subrow_iter = map[temp_dom.parent_element].visual_node.content.table
                                .add_new_row(null, temp_dom.node_name, temp_dom.text_content);  
                        else 
                            subrow_iter = map[temp_dom.parent_element].visual_node.content.table
                                .add_new_row(null, temp_dom.node_name, ""); 

                        if(!attr.is_empty) {
                            foreach(var itt in attr.entries){
                                map[temp_dom.parent_element].visual_node.content.table
                                    .add_attrib_for_child(subrow_iter, itt.key, itt.value.node_value); //CHANGE
                            }
                        }
                    }
                }

                // BFS ADD CHILDS TO ОБХОД
                var node = main_array.poll();
                foreach (var item in node.child_nodes)
                    if (item is GXml.DomElement){
                        temp_array.add(item);// добавление тех кого еще нужно будет обойти
                    }
                //  temp_array.add_all(node.child_nodes);
            }
            main_array.add_all(temp_array);
            temp_array.clear();// можно наверн оптимизировать сделав temp стаком?
        }
    }
    /*
    * C-All descendants have no descendants
    * A - Each descendant has descendants
    * B-Has at least one descendant that has no descendants and has a descendant that has descendants
    */
    NodeType type_recognition(GXml.DomElement e){
        var (a,c) = new bool[] {true,true,true};
        
        foreach (var child in e.child_nodes){
            if (child is GXml.DomElement){
                // Если хотя бы один потомок имеет потомков то это уже не C
                if (((GXml.DomElement)child).child_element_count != 0)
                    c = false; 
                else 
                    a = false;
            }
        }

        if (!a && !c) return B;
        if (a && !c) return A;
        if (c && !a) return C;
        assert_not_reached();
    }
    
    //WARNING возможно копирование аргумента
    inline Node add_to_node_view(NodeType node_type, ref GXml.DomElement e, string node_name){
        var new_node1 = new_node(node_type, ref e, node_name);

        nv.add_with_child(new_node1.visual_node.gtk_node,
                          new_node1.visual_node.content);

        nv.set_node_position(new_node1.visual_node.gtk_node, new_node_pos.x, new_node_pos.y);
        Gtk.Allocation alloc = nv.get_node_allocation(new_node1.visual_node.gtk_node);

		new_node1.width = alloc.width; new_node1.height = alloc.height;
            

        return new_node1;
    }

        //  public int find_nodes_index_by_data(string search_data){
    //      for (int i = 0; i < nodes.length; i++)
    //          if (nodes[i] != null && nodes[i].data == search_data)
    //              return i;

    //      warning("item not found");
    //      return -1;
    //  }

        //  public bool simple_symmetry_chain_check(Node node1, Node node2){
    //      bool first_in_second = false;
    //      bool second_in_first = false;
    //      foreach (Chain item in node1.potomki)
    //          if (node2.pos == item.index)
    //              second_in_first = true;

    //      foreach (Chain item in node2.predki)
    //          if (node1.pos == item.index)
    //              first_in_second = true;

    //      prin("symmetry check ", first_in_second," ", second_in_first);
    //      return first_in_second && second_in_first;
    //  }

    //  public void delete_predka(Node n1, Node n2){
    //      foreach (var item in n1.predki)
    //          if (item.index == n2.pos)
    //              n1.predki.remove(item);
    //  }
}